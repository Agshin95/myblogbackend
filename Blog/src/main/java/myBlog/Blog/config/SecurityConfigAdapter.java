package myBlog.Blog.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private static final String USER_API_V1 = "/api/v1/user/**";
    private static final String POST_API_V1 = "/api/v1/post**";
    private static final String COMMENT_API_V1 = "/api/v1/comment**";
    private final UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST,USER_API_V1).anonymous()
                .antMatchers(HttpMethod.PUT, USER_API_V1).permitAll()
                .antMatchers(HttpMethod.GET, USER_API_V1).permitAll()
                .antMatchers(HttpMethod.DELETE, USER_API_V1).permitAll()
                .antMatchers(HttpMethod.POST, POST_API_V1).hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.PUT, POST_API_V1).hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.GET, POST_API_V1).hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.DELETE, POST_API_V1).hasAnyRole("USER","ADMIN")
                .antMatchers(HttpMethod.POST, COMMENT_API_V1).hasAnyRole( "USER","ADMIN")
                .antMatchers(HttpMethod.PUT, COMMENT_API_V1).hasAnyRole( "USER","ADMIN")
                .antMatchers(HttpMethod.GET, COMMENT_API_V1).hasAnyRole( "USER","ADMIN")
                .antMatchers(HttpMethod.DELETE, COMMENT_API_V1).hasAnyRole( "USER","ADMIN");


        super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST,USER_API_V1);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
