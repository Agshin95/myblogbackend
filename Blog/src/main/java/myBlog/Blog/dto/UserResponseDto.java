package myBlog.Blog.dto;

import lombok.Data;

@Data
public class UserResponseDto {
    private Long id;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String email;
}
