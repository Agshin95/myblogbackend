package myBlog.Blog.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class CommentRequestDto {
    private Long id;
    @Email
    @NotEmpty
    private String email;
    private String description;
}
