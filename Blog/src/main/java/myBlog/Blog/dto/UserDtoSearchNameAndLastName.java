package myBlog.Blog.dto;

import lombok.Data;

@Data
public class UserDtoSearchNameAndLastName {
    private Long id;
    private String name;
    private String lastName;
}
