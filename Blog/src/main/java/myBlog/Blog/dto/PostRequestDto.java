package myBlog.Blog.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class PostRequestDto {
    private Long id;
    private String title;
    private String description;

    @Email
    @NotEmpty
    private String email;
}
