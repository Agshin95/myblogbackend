package myBlog.Blog.dto;

import lombok.Data;

@Data
public class PostResponseDto {
    private Long id;
    private String title;
    private String description;
    private UserDtoForCommentAndPostResponse userDtoForCommentAndPostResponse;
}
