package myBlog.Blog.dto;

import lombok.Data;

@Data
public class CommentResponseDto {
    private Long id;
    private String description;
    private UserDtoForCommentAndPostResponse userDtoForCommentAndPostResponse;
}
