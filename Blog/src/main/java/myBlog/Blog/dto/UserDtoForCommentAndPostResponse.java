package myBlog.Blog.dto;

import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDtoForCommentAndPostResponse {
    private Long id;
    private String name;
    private String lastName;
}
