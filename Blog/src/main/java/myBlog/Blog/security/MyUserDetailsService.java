package myBlog.Blog.security;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        MyUserDetails myUserDetails = userRepository.findByEmail(email).
                map(MyUserDetails::new)
                .orElseThrow(RuntimeException::new);
        return myUserDetails;
    }
}
