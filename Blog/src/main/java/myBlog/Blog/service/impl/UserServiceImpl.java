package myBlog.Blog.service.impl;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.UserDtoSearchNameAndLastName;
import myBlog.Blog.dto.UserRequestDto;
import myBlog.Blog.dto.UserResponseDto;
import myBlog.Blog.exception.ExistingUserException;
import myBlog.Blog.exception.UserNotFoundException;
import myBlog.Blog.model.Users;
import myBlog.Blog.repository.RoleRepository;
import myBlog.Blog.repository.UserRepository;
import myBlog.Blog.service.RegisteredUserService;
import myBlog.Blog.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleRepository roleRepository;
    private final RegisteredUserService registeredUserService;

    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        if (registeredUserService.registeredUser(userRequestDto.getEmail())) {
            Users user = modelMapper.map(userRequestDto, Users.class);
            user.setPassword(bCryptPasswordEncoder.encode(userRequestDto.getPassword()));
            user.setRoles(List.of(roleRepository.findByName("ROLE_USER")));
            Users saveUser = userRepository.save(user);
            return modelMapper.map(saveUser, UserResponseDto.class);
        } else throw new ExistingUserException();
    }

    @Override
    public UserResponseDto getUser(Long id) {
        return modelMapper.map(userRepository.findById(id).orElseThrow(UserNotFoundException::new)
                , UserResponseDto.class);
    }

    @Override
    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto) {
        Users users = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRequestDto.setId(users.getId());
        users.setRoles(List.of(roleRepository.findByName("ROLE_USER")));
        users.setPassword(bCryptPasswordEncoder.encode(userRequestDto.getPassword()));
        Users saveUser = userRepository.save(users);
        return modelMapper.map(saveUser, UserResponseDto.class);
    }

    @Override
    public String deletedUser(Long id) {
        Users users = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRepository.delete(users);
        return "User deleted";
    }

    @Override
    public List<UserResponseDto> searchUser(UserDtoSearchNameAndLastName nameAndLastName) {
        List< Users >users = userRepository.findByNameAndLastName(nameAndLastName.getName(), nameAndLastName.getLastName());

        UserResponseDto userResponseDto = modelMapper.map(users, UserResponseDto.class);
        return List.of(userResponseDto);
    }
}
