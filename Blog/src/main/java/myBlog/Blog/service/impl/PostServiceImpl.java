package myBlog.Blog.service.impl;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.PostRequestDto;
import myBlog.Blog.dto.PostResponseDto;
import myBlog.Blog.dto.UserDtoForCommentAndPostResponse;
import myBlog.Blog.exception.PostRecurseNotFoundException;
import myBlog.Blog.exception.UserNotFoundException;
import myBlog.Blog.model.Post;
import myBlog.Blog.model.Users;
import myBlog.Blog.repository.PostRepository;
import myBlog.Blog.repository.UserRepository;
import myBlog.Blog.service.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;


@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public PostResponseDto getPost(Long id) {
        return modelMapper.map(postRepository.findById(id).orElseThrow(PostRecurseNotFoundException::new)
                , PostResponseDto.class);
    }

    @Override
    public String deletedPost(Long id) {
        Post post = postRepository.findById(id).orElseThrow(PostRecurseNotFoundException::new);
        postRepository.delete(post);
        return "Post deleted";
    }

    @Override
    @Transactional
    public PostResponseDto createPost(PostRequestDto requestDto) {
        Users users = userRepository.findByEmail(requestDto.getEmail()).
                orElseThrow(UserNotFoundException::new);
        Post post = modelMapper.map(requestDto, Post.class);
        post.setUsers(users);
        post.setDate(LocalDate.now());
        modelMapper.map(users, UserDtoForCommentAndPostResponse.class);
        Post save = postRepository.save(post);
        return modelMapper.map(save,PostResponseDto.class);
    }

    @Override
    public PostResponseDto updatePost(Long id, PostRequestDto requestDto) {
        Post post = postRepository.findById(id).orElseThrow(PostRecurseNotFoundException::new);
        requestDto.setId(post.getId());
        post.setUpdateDate(LocalDate.now());
        post.setDate(LocalDate.now());
        Post save = postRepository.save(modelMapper.map(requestDto, Post.class));
        return modelMapper.map(save,PostResponseDto.class);

    }
}
