package myBlog.Blog.service.impl;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.CommentRequestDto;
import myBlog.Blog.dto.CommentResponseDto;
import myBlog.Blog.dto.PostResponseDto;
import myBlog.Blog.dto.UserDtoForCommentAndPostResponse;
import myBlog.Blog.exception.CommentNotFoundException;
import myBlog.Blog.exception.PostRecurseNotFoundException;
import myBlog.Blog.exception.UserNotFoundException;
import myBlog.Blog.model.Comment;
import myBlog.Blog.model.Post;
import myBlog.Blog.model.Users;
import myBlog.Blog.repository.CommentRepository;
import myBlog.Blog.repository.PostRepository;
import myBlog.Blog.repository.UserRepository;
import myBlog.Blog.service.CommentService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final ModelMapper modelMapper;

    @Override
    public CommentResponseDto getComment(Long id) throws CommentNotFoundException {
        return modelMapper.map(commentRepository.findById(id).orElseThrow(CommentNotFoundException::new)
                , CommentResponseDto.class);
    }

    @Override
    public String deletedComment(Long id) throws CommentNotFoundException {
        Comment comment = commentRepository.findById(id).orElseThrow(CommentNotFoundException::new);
        commentRepository.delete(comment);
        return "Comment deleted";
    }

    @Override
    @Transactional
    public CommentResponseDto createComment(Long postId, CommentRequestDto requestDto) {
        Post post = postRepository.findById(postId).orElseThrow(PostRecurseNotFoundException::new);
        Users user = userRepository.findByEmail(requestDto.getEmail()).orElseThrow(UserNotFoundException::new);
        Comment comment = modelMapper.map(requestDto, Comment.class);
        comment.setDate(LocalDate.now());
        comment.setPosts(post);
        comment.setUsers(user);
        Comment save = commentRepository.save(comment);
        modelMapper.map(user, UserDtoForCommentAndPostResponse.class);
        return modelMapper.map(save, CommentResponseDto.class);
    }

    @Override
    public CommentResponseDto updateComment(Long commentId, CommentRequestDto requestDto) throws CommentNotFoundException {
        Comment comment = commentRepository.findById(commentId).orElseThrow(CommentNotFoundException::new);
        requestDto.setId(comment.getId());
        comment.setUpdateDate(LocalDate.now());
        Comment map = modelMapper.map(requestDto, Comment.class);
        Comment save = commentRepository.save(map);
        return modelMapper.map(save, CommentResponseDto.class);
    }
}
