package myBlog.Blog.service.impl;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.exception.UserNotFoundException;
import myBlog.Blog.model.Users;
import myBlog.Blog.repository.UserRepository;
import myBlog.Blog.service.RegisteredUserService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegisteredUserServiceImpl implements RegisteredUserService {

    private final UserRepository userRepository;

    @Override
    public Boolean registeredUser(String email) {
        if (userRepository.findByEmail(email).isEmpty()){
            return true;
        }
        else return false;
    }
}
