package myBlog.Blog.service;

import myBlog.Blog.dto.CommentRequestDto;
import myBlog.Blog.dto.CommentResponseDto;
import myBlog.Blog.exception.CommentNotFoundException;

public interface CommentService {

    CommentResponseDto getComment(Long id) throws CommentNotFoundException;

    String deletedComment(Long id) throws CommentNotFoundException;

    CommentResponseDto createComment(Long postId, CommentRequestDto requestDto);

    CommentResponseDto updateComment(Long commentId,CommentRequestDto requestDto) throws CommentNotFoundException;
}
