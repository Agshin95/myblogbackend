package myBlog.Blog.service;

import myBlog.Blog.dto.UserDtoSearchNameAndLastName;
import myBlog.Blog.dto.UserRequestDto;
import myBlog.Blog.dto.UserResponseDto;

import java.util.List;

public interface UserService {

     UserResponseDto createUser(UserRequestDto userRequestDto);

     UserResponseDto getUser(Long id);

     UserResponseDto updateUser(Long id,UserRequestDto userRequestDto);

     String deletedUser(Long id);

     List<UserResponseDto> searchUser(UserDtoSearchNameAndLastName userDtoSearchNameAndLastName);
}
