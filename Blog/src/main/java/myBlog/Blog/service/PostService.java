package myBlog.Blog.service;

import myBlog.Blog.dto.PostRequestDto;
import myBlog.Blog.dto.PostResponseDto;

public interface PostService {

    PostResponseDto getPost(Long id);

    String deletedPost(Long id);

    PostResponseDto createPost(PostRequestDto requestDto);

    PostResponseDto updatePost(Long id, PostRequestDto requestDto);
}
