package myBlog.Blog.repository;

import myBlog.Blog.dto.UserResponseDto;
import myBlog.Blog.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<Users,Long> {
    Optional<Users> findByEmail(String email);
    List<Users> findByNameAndLastName(String name, String lastName);


}
