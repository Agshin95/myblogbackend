package myBlog.Blog.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String description;

    private LocalDate date;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private Users users;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "post_id",referencedColumnName = "id")
    private Post posts;
}
