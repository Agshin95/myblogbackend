package myBlog.Blog.exception;

public class ExistingUserException extends RuntimeException {
    private static final long serialVersionUID = 58432532487812L;

    private static final String MESSAGE = "There is a user with this email";

    public ExistingUserException() {
        super(MESSAGE);
    }
}
