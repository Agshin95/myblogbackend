package myBlog.Blog.exception;

public class UserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 58432532487811L;

    private static final String MESSAGE = "User not found in the system";

    public UserNotFoundException() {
        super(MESSAGE);
    }
}
