package myBlog.Blog.exception;

public class CommentNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 58432532487855L;

    private static final String MESSAGE = "Comment not found in the system";

    public CommentNotFoundException() {
        super(MESSAGE);
    }
}
