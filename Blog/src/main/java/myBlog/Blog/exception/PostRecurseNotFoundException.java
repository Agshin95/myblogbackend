package myBlog.Blog.exception;

public class PostRecurseNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 58432532487912L;

    private static final String MESSAGE = "Post recurse not found in the system!";

    public PostRecurseNotFoundException() {
        super(MESSAGE);
    }
}
