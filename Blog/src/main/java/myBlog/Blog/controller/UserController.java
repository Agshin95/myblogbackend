package myBlog.Blog.controller;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.UserDtoSearchNameAndLastName;
import myBlog.Blog.dto.UserRequestDto;
import myBlog.Blog.dto.UserResponseDto;
import myBlog.Blog.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/signUp")
    public UserResponseDto createUser(@RequestBody UserRequestDto requestDto) {
        return userService.createUser(requestDto);
    }

    @PutMapping("{userId}/update")
    public UserResponseDto updateUser(@PathVariable Long userId, @RequestBody UserRequestDto userRequestDto) {
        return userService.updateUser(userId, userRequestDto);
    }

    @GetMapping("/{userID}")
    public UserResponseDto getUser(@PathVariable Long userID) {
        return userService.getUser(userID);
    }

    @DeleteMapping("/deleted/{id}")
    public String deletedUser(@PathVariable Long userID) {
        return userService.deletedUser(userID);
    }

    @GetMapping("/search")
    public List<UserResponseDto> SearchByNameAndSurname(@RequestBody UserDtoSearchNameAndLastName nameAndLastName) {
        return userService.searchUser(nameAndLastName);
    }

}
