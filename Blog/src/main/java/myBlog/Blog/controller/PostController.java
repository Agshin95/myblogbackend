package myBlog.Blog.controller;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.PostRequestDto;
import myBlog.Blog.dto.PostResponseDto;
import myBlog.Blog.service.PostService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/post")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;

    @PostMapping("/create")
    public PostResponseDto createPost(@RequestBody PostRequestDto requestDto){
        return postService.createPost(requestDto);
    }
    @PutMapping("{postID}/update")
    public PostResponseDto updatePost(@PathVariable Long postID, @RequestBody PostRequestDto requestDto){
        return postService.updatePost(postID,requestDto);
    }

    @GetMapping("/{postID}")
    public PostResponseDto getPost(@PathVariable Long postID){
        return postService.getPost(postID);
    }

    @DeleteMapping("/deleted/{postID}")
    public String deletedPost(@PathVariable Long postID){
        return postService.deletedPost(postID);
    }
}
