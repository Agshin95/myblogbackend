package myBlog.Blog.controller;

import lombok.RequiredArgsConstructor;
import myBlog.Blog.dto.CommentRequestDto;
import myBlog.Blog.dto.CommentResponseDto;
import myBlog.Blog.service.CommentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/comment")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping("/{postID}/create")
    public CommentResponseDto createComment(@PathVariable Long postID,
                                            @RequestBody CommentRequestDto requestDto) {

        return commentService.createComment(postID,requestDto);
    }

    @PutMapping("/{commentID}/update")
    public CommentResponseDto updateComment(@PathVariable Long commentID,
                                            @RequestBody CommentRequestDto requestDto) {
        return commentService.updateComment(commentID, requestDto);
    }

    @GetMapping("/{commentID}")
    public CommentResponseDto getComment(@PathVariable Long commentID){
        return commentService.getComment(commentID);
    }

    @DeleteMapping("/deleted/{commentID}")
    public String deletedComment(@PathVariable Long commentID){
        return commentService.deletedComment(commentID);
    }
}
